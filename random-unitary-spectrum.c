#include "util.h"
#include "random-unitary.h"

int main(int argc, char const *argv[])
{
    int i;
    int N;
    int samplesize;
    int sample_id;
    clock_t start;
    struct timespec ts;
    FILE *output_file;
    char output_filename[2048];
    double _Complex *U;
    double _Complex *U_eigenvalues;
    double *U_eigenvalues_arg;

    start = clock();

    if (timespec_get(&ts, TIME_UTC) == 0)
    {
        fprintf(stderr, "\n\nCannot determine calendar time to seed random number generator.\n");
        exit(EX_OSERR);
    }
    srandom(ts.tv_nsec ^ ts.tv_sec);

    if(argc != 3)
    {
        printf("Usage: %s N samplesize\n\n", argv[0]);
        printf("Calculates eigenvalues of random unitary matrix.\n");
        printf("\n\tN\tDimension of unitary matrix\n");
        printf("\n\tsamplesize\tNumber of random samples\n");
        exit(EX_USAGE);
    }

    N = atoi(argv[1]);
    samplesize = atoi(argv[2]);

    if(samplesize < 1 || N < 1)
    {
        fprintf(stderr, "\n\n Parameters should be positive integers.\n");
        exit(EX_USAGE);
    }

    U = (double _Complex *) my_calloc(N * N, sizeof(double _Complex));
    U_eigenvalues = (double _Complex *) my_calloc(N, sizeof(double _Complex));
    U_eigenvalues_arg = (double *) my_calloc(N, sizeof(double));

    for(sample_id = 0; sample_id < samplesize; sample_id++)
    {
        printf("\n==== Sample %i/%i ====\n", sample_id+1, samplesize);

        printf("\nCalculating random unitary... ");
        get_Haar_random_unitary(U, N);

        printf("\nDiagonalizing... ");
        diagonalize(U, N, U_eigenvalues);

        for(i = 0; i < N; i++)
        {
            U_eigenvalues_arg[i] = carg(U_eigenvalues[i]);
        }

        // Store data

        snprintf(output_filename, sizeof(output_filename), "eigenvalues-%i.txt", N);
        output_file = fopen(output_filename, "a");

        if(flock(fileno(output_file), LOCK_EX))
        {
            fprintf(stderr, "\n\nCould not lock file %s for writing: Error %i\n", output_filename, errno);
            exit(EX_OSERR);
        }

        for(i = 0; i < N; i++)
        {
            fprintf(output_file, "%g", U_eigenvalues_arg[i]);
            if(i != N-1)
            {
                fprintf(output_file, " ");
            }
        }
        fprintf(output_file, "\n");
        
        flock(fileno(output_file), LOCK_UN);
        fclose(output_file);
    }

    free(U_eigenvalues_arg);
    free(U_eigenvalues);
    free(U);

    printf("\nTotal time taken ");
    report_elapsed_time(start);
    exit(EX_OK);
}