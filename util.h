/*  Copyright 2021 Niklas Bölter <niklas.boelter@theorie.physik.uni-goettingen.de> */

#ifndef CLUSTER_TUTORIAL_UTIL_H
#define CLUSTER_TUTORIAL_UTIL_H

#include <complex.h>
#include <errno.h>
#include <limits.h>
#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sysexits.h>
#include <time.h>

#include <sys/file.h>

#ifdef USE_INTEL_MKL
#define MKL_Complex16 double _Complex
#include <mkl.h>
#else
#include <lapacke.h>
#endif

#ifndef lapack_int
#define lapack_int MKL_INT
#endif

extern clock_t start;
extern int argv_len; // Used for status reporting
extern char *argv_ptr;
extern char *status_prefix; // Prefix string for status reporting

void * my_calloc(long long count, size_t size);
void * my_memset(void *b, int c, long long count, size_t size);
void diagonalize(double _Complex *M, lapack_int dim, double _Complex *eigenvalues);
void check_unitary(const double _Complex *U, long long N);
void report_elapsed_time(clock_t start);

#endif

