#!/usr/bin/env python3
from matplotlib import pyplot
import numpy
import sys

π = numpy.pi


def CUE_distribution(s):
    return 2 * (4 * s / numpy.pi)**2 * numpy.exp(-4 / numpy.pi * s**2)


N = int(sys.argv[1])

eigenvalues = numpy.loadtxt(f"eigenvalues-{N}.txt")

eigenvalues.sort()

eigenvalues_spacing = numpy.abs(eigenvalues[:, 1:] - eigenvalues[:, :-1]) * N / (2 * π)

pyplot.ylabel(r"Eigenvalue Density")
pyplot.xlabel(r"Eigenvalue Argument")

pyplot.hist(eigenvalues.flatten(), bins=31, density=True, label=rf"$N = {N}$")
pyplot.plot([-π, π], [1 / (2 * π), 1 / (2 * π)], label=r"$1/2\pi$")

pyplot.legend()
pyplot.title(f"Random Unitary Matrix, Samplesize {eigenvalues.shape[0]}")
filename = f"eigenvalue-frequency-N{N}.png"
pyplot.savefig(filename, dpi=300)
print(f"Written {filename}")
pyplot.clf()


pyplot.ylabel(r"Eigenvalue Spacing Density $\rho(s)$")
pyplot.xlabel(r"Eigenvalue Spacing $s$")

pyplot.hist(eigenvalues_spacing.flatten(), bins=50, density=True, label=rf"$N = {N}$")
s = numpy.linspace(0, 3, 2000)
pyplot.plot(s, CUE_distribution(s), label="CUE(s)")

pyplot.legend()
pyplot.title(f"Random Unitary Matrix, Samplesize {eigenvalues.shape[0]}")
filename = f"eigenvalue-spacing-N{N}.png"
pyplot.savefig(filename, dpi=300)
print(f"Written {filename}")
