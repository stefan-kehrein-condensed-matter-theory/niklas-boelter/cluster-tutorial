/*  Copyright 2019 Niklas Bölter <niklas.boelter@theorie.physik.uni-goettingen.de> */

#ifndef RANDOM_UNITARY_H
#define RANDOM_UNITARY_H

#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <time.h>
#include <math.h>
#include <sysexits.h>
#include <complex.h>

#include <sys/types.h>
#include <sys/stat.h>

#include "util.h"

void get_Haar_random_unitary(double _Complex *U, lapack_int N);
void get_diagonal_unitary(double _Complex *U, lapack_int N);
void get_identity_unitary(double _Complex *U, lapack_int N);

#endif
