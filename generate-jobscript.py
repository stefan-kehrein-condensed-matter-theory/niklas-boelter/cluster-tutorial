#!/usr/bin/env python3
import getpass
import os
import sys

N = int(sys.argv[1])
samplesize = int(sys.argv[2])

filename = f"cluster-tutorial-random-unitary-spectrum-{N}-{samplesize}.condor"
condor_script = """\
#!/usr/bin/condor_submit
N                = {0}
samplesize       = {1}
executable       = cluster-tutorial-random-unitary-spectrum.sh
arguments        = "$(N) $(samplesize) $(ClusterID)-$(ProcID)"
log              = cluster-tutorial-random-unitary-spectrum-$(N)-$(samplesize)-$(ClusterId)-$(ProcId).log
output           = cluster-tutorial-random-unitary-spectrum-$(N)-$(samplesize)-$(ClusterId)-$(ProcID).out
error            = cluster-tutorial-random-unitary-spectrum-$(N)-$(samplesize)-$(ClusterID)-$(ProcID).err
request_memory   = 1GB
request_disk     = 10MB
+RequestFreeDisk = 10240
+MaxRuntime      = 86400
requirements     = CpuFamily == 23 && CpuModelNumber == 49
notify_user      = {2}@theorie.physik.uni-goettingen.de
notification     = Error
queue
"""

with open(filename, "w") as f:
    f.write(condor_script.format(N, samplesize, getpass.getuser()))
    os.chmod(filename, 0o755)
    print(f"Written {filename}")
