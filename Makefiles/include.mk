OBJECTS=util.o random-unitary.o

GIT_DESCRIBE=$(shell git describe --always --dirty --tags)
CPPFLAGS+=-DGIT_DESCRIBE="\"$(GIT_DESCRIBE)\""

random-unitary-spectrum: random-unitary-spectrum.c $(OBJECTS)
	$(CC) -o $@ $^ $(CFLAGS) $(CPPFLAGS) $(LDFLAGS)

clean:
	rm -f random-unitary-spectrum $(OBJECTS)
