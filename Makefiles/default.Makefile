CC=gcc
CFLAGS=-O2 -g -march=native -Wall
CPPFLAGS=-isystem/usr/local/opt/lapack/include -isystem/usr/local/opt/lapack/include
LDFLAGS=-llapack

include Makefiles/include.mk
