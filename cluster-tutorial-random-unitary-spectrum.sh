#!/bin/bash

if [ $# -ne 3 ]
then
    echo "Usage: ${0} <N> <sample size> <JobID>" >&2
    exit
fi

pwd=$(pwd)
N=${1}
samplesize=${2}
job_id=${3}

cleanup() {
    echo "Received SIGTERM, saving data to ${pwd}/unfinished-job-data/${job_id}" >&2
    mkdir -p "${pwd}/unfinished-job-data"
    cp -r ${TMPDIR} "${pwd}/unfinished-job-data/${job_id}"
    pkill -P $$ # kill children
    exit -1
}
trap cleanup SIGTERM

cd ${TMPDIR}

$pwd/random-unitary-spectrum ${N} ${samplesize}

mkdir -p ~/cluster-tutorial/cluster-tutorial-random-unitary-spectrum-${N}-${samplesize}-${job_id}/

cp -v * ~/cluster-tutorial/cluster-tutorial-random-unitary-spectrum-${N}-${samplesize}-${job_id}/
