/*  Copyright 2019 Niklas Bölter <niklas.boelter@theorie.physik.uni-goettingen.de> */

#include "util.h"


void * my_calloc(long long count, size_t size)
{
    void *ptr;

    ptr = calloc((size_t) count, size);
    
    #ifdef DEBUG
    printf("\nCalled my_calloc(%lli, %zu) = %p\n", count, size, ptr);
    #endif

    return ptr;
}


void * my_memset(void *b, int c, long long count, size_t size)
{
    #ifdef DEBUG
    printf("\nCalled my_memset(%p, %i, %lli, %zu)\n", b, c, count, size);
    #endif
    
    memset(b, c, (size_t) count * size);

    return b;
}


void diagonalize(double _Complex *M, lapack_int dim, double _Complex *eigenvalues)
{
    /* WARNING: LAPACK USES COLUMN MAJOR ORDER */

    /* Lapacke variables */
    lapack_int ret, lwork;
    double _Complex workopt;
    double _Complex *work;
    double *rwork;
    lapack_int dvr = 1;

    clock_t start = clock();

    my_memset(eigenvalues, 0, dim, sizeof(double _Complex));

    rwork = my_calloc(2 * dim, sizeof(double));

    if(!rwork)
    {
        fprintf(stderr, "Could not allocate memory.");
        exit(EX_OSERR);
    }

    lwork = -1;
    zgeev_("None", "None", &dim, M, &dim, eigenvalues, 
        NULL, &dvr, NULL, &dvr, &workopt, &lwork, rwork, &ret);

    if((int)ret != 0)
    {
        fprintf(stderr, "ERROR Workspace query failed. Return Code: %i\n", (int) ret);
        exit(EX_SOFTWARE);
    }

    lwork = (int) workopt;

    work = my_calloc(lwork, sizeof(double _Complex));

    if(!work)
    {
        fprintf(stderr, "Could not allocate memory.");
        exit(EX_OSERR);
    }

    zgeev_("None", "None", &dim, M, &dim, eigenvalues, 
        NULL, &dvr, NULL, &dvr, work, &lwork, rwork, &ret);

    free(rwork);
    free(work);

    if((int)ret != 0)
    {
        fprintf(stderr, "ERROR Diagonalization failed. Return Code: %i\n", (int) ret);
        exit(EX_SOFTWARE);
    }
    report_elapsed_time(start);
}


void check_unitary(const double _Complex *U, long long N)
{
    int i,j,k;
    double _Complex matrixElement;

    for(i = 0; i < N; i++)
    {
        for(j = 0; j < N; j++)
        {
            matrixElement = 0.0 + 0.0 * (double _Complex) _Complex_I;
            for(k = 0; k < N; k++)
            {
                matrixElement += U[i + N * k] * conj(U[j + N * k]);
            }

            if((i == j && cabs(matrixElement - 1.0) > 1E-12) || 
               (i != j && cabs(matrixElement) > 1E-12))
            {
                fprintf(stderr, "\nError: O is not unitary!\n");
                exit(EX_SOFTWARE);
            }
        }
    }
    printf("\nUnitary ✔︎\n");
}


void report_elapsed_time(clock_t start)
{
    clock_t stop = clock();
    long ms = (stop - start) * 1000 / CLOCKS_PER_SEC;

    if(ms > 1000*60*60)
    {
        printf("# Time taken: %4ld hours %2ld minutes %2ld seconds %4ld milliseconds\n",
        ((ms/1000)/60)/60, ((ms/1000)/60)%60, (ms/1000)%60, ms%1000);
    }
    else if(ms > 1000*60)
    {
        printf("# Time taken: %2ld minutes %2ld seconds %4ld milliseconds\n",
        ((ms/1000)/60)%60, (ms/1000)%60, ms%1000);
    }
    else
    {
        printf("# Time taken: %2ld seconds %4ld milliseconds\n",
        (ms/1000)%60, ms%1000);
    }

}
