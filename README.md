# Cluster Tutorial

## Gitlab setup

If you don't have a ssh key yet you can create it with:

```bash
ssh-keygen -t ed25519
```

Confirm the standard location `~/.ssh/id_ed25519` and provide a secure passphrase, then upload the contents of `~/.ssh/id_ed25519.pub` to gitlab.gwdg.de under Preferences > SSH Keys.

To clone the tutorial project you can now type:

```bash
git clone git@gitlab.gwdg.de:stefan-kehrein-condensed-matter-theory/niklas-boelter/cluster-tutorial.git
```

Now you can enter the project directory with:
```bash
cd cluster-tutorial
```

## Run on your machine

To get the program up and running on your personal machine you should install lapack (for example with HomeBrew on macOS) and then use the following commands in the project directory:

```bash
ln -s Makefiles/default.Makefile Makefile
make
./random-unitary-spectrum 128 10
./plot-random-unitary-spectrum.py 128
```

To delete the compiled object files use the following command:

```bash
make clean
```

## Run a job on GoeGrid

To compile a binary for running on the AMD EPYC nodes of GoeGrid run the following on ds10 in the project directory:

```bash
source /scratch/AG-Kehrein/setenv_AOCC.sh
ln -s Makefiles/ds10.Makefile Makefile
make
```

You can now create a directory on rocks to submit your jobs from by running the following commands on ds10 in the project directory:
```bash
mkdir /net/theorie/rocks/$USER/cluster-tutorial
cp generate-jobscript.py cluster-tutorial-random-unitary-spectrum.sh random-unitary-spectrum /net/theorie/rocks/$USER/cluster-tutorial
```

Connect to rocks and generate and submit a PBS job:
```bash
ssh rocks
cd cluster-tutorial
./generate-jobscript.py 256 100
./cluster-tutorial-random-unitary-spectrum-256-100.condor
```

After the job is complete there should be a new directory cluster-tutorial/cluster-tutorial-random-unitary-spectrum-256-100-[ClusterID]-[JobID] with the results. You can copy the `eigenvalues-256.txt` file to the project directory on your local computer and run `./plot-random-unitary-spectrum.py 256`.

![Eigenvalues of random unitary matrices](https://www.theorie.physik.uni-goettingen.de/~niklas.boelter/eigenvalue-frequency-N256.png)

![Eigenvalue spacing of random unitary matrices](https://www.theorie.physik.uni-goettingen.de/~niklas.boelter/eigenvalue-spacing-N256.png)
